Dieses Programm führt eine Vektoroperation aus - einmal sequentiell und einmal parallel mit OpenMP.
Dabei werden die Laufzeiten mit verschiedenen Vektorgrößen gemessen und verglichen.
Es fällt auf, dass der Speedup deutlich größer ist als bei meiner Implementierung mit pThreads.
(33 vs 57)


Sie muessen pro Sitzung / Job-Lauf einmalig ausfuehren:
   module load gcc

Uebersetzen mit:
    make

Loeschen des ausfuehrbaren Programms:
    make clean

Testen auf Plausibilitaet mit kleinen Vektoren:
    make test

Ausfuehren von Messungen mit unterschiedlichen Threadzahlen auf grossen Vektoren
(  ***** NICHT AUF wr0!!! *****):
    make run

Starten des Job-Skripts:
    sbatch job_vector.sh



Die fuer Sie interessante Methode ist: vectorInit und vectorOperationParallel. Nur dort duerfen Sie Aenderungen vornehmen.
Alle weiteren Randbedingungen ergeben sich auf der Aufgabenstellung.
