#!/bin/bash
#SBATCH --partition wr43         # partition (queue)
#SBATCH --nodes=1                # number of nodes
#SBATCH --ntasks-per-node=96     # number of cores per node
#SBATCH --mem 80G                # memory per node in MB (different units with suffix K|M|G|T)
#SBATCH --time 30:00             # total runtime of job allocation (format D-HH:MM:SS)
#SBATCH --output slurm.%N.%j.out # STDOUT (%N: nodename, %j: job-ID)
#SBATCH --error slurm.%N.%j.err  # STDERR

echo "Programm wird ausgeführt auf " `hostname`

module load intel-compiler

# sequentiell ausführen ohne Grafikausgabe
make runSeq2

# parallel ausführen ohne Grafikausgabe
make run2

exit
