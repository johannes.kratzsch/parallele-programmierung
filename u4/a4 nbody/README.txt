Hierbei handelt es sich um die Simulation eines N-Körperproblems.
Der sequentielle Durchlauf dauert 26,78s. Der parallele Algorithmus braucht
zunächst deutlich länger (70s bei einem Thread), wird bei steigender Threadzahl
dann aber immer schneller. Bei 64 Threads läuft das Porgramm 21,83s und liefert so
einen Speedup von 1,22. Mehr Details dazu befinden sich in der Ausgabe vom
Jobscript.

Pro Session einmalig "module load intel-compiler" ausführen.

make run: parallel mit grafischer Ausgabe
make run2: parallel ohne grafische Ausgabe

make runSeq: sequentiell mit grafischer Ausgabe
make run2: sequentiell ohne grafische Ausgabe