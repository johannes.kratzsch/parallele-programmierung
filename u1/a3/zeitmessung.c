#include <stdio.h>
#include <libFHBRS.h>
#define N 30000

extern double gettime(); //Schlüsselwort extern ist optional

double matrix[N][N];
/*
     * Als double-matrix, also je 8 Byte, sind es 6867 MB (30000^2 * 8 / 1024^2).
     * Dies kann man auch bei Ausführen den Programms feststellen, wenn man die RAM Usage mit
     * htop ermittelt.
     * => zu groß für den Stack! Es kommt zum Speicherzugriffsfehler.
     * Daher Schlüsselwort static benutzen oder außerhalb von Block deklieren,
     * damit die Matrix im Datensegment abgelegt wird.
     * Als statische Variable bekomme ich einen Kompilierungsfehler,
     * daher mache ich hier eine globale Variable drauß.
     *
     * */

int main (int argc, char **argv) {
    double summe = 0;
    double startzeitpunkt = gettime();

    for (int i = 0; i < N; i++) {
        for(int j = 0; j < N; j++) {
            matrix[i][j] = i + j + 1;
        }
    }


    for (int i = 0; i < N; i++) {
        for(int j = 0; j < N; j++) {
            summe += (int)matrix[i][j];
        }
    }

    double rechenzeit = gettime() - startzeitpunkt;

    printf("Die Summe ist %.1f und die Berechnung dauerte %.2f Sekunden \n", summe, rechenzeit);

    return 0;
}
