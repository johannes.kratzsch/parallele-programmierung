#!/bin/bash
#SBATCH --partition=wr13          # partition (queue)
#SBATCH --nodes=1                # number of nodes
#SBATCH --ntasks-per-node=272     # number of cores per node
#SBATCH --mem=8G                 # memory per node in MB (different units with suffix K|M|G|T)
#SBATCH --time=2:00              # total runtime of job allocation (format D-HH:MM:SS; first parts optional)
#SBATCH --output=slurm.%j.out    # filename for STDOUT (%N: nodename, %j: job-ID)
#SBATCH --error=slurm.%j.err     # filename for STDERR

hostname # Rechnernamen auf Konsole ausgeben

./zeitmessung.exe

exit