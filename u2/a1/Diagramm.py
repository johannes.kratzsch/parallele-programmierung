import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv('messergebnisse.csv')

fig,ax = plt.subplots(ncols=1,nrows=1,figsize=(15,5))
plt.rcParams.update({'font.size': 25})

plt.plot(data.p, data.sequential_time, label='sequential_time')
plt.plot(data.p, data.parallel_time, label='parallel_time')

ax.grid(True)
ax.set_xlabel("Anzahl Threads")
ax.set_ylabel("Ausführungszeit in s")
ax.legend()


fig1,ax1 = plt.subplots(ncols=1,nrows=1,figsize=(15,5))

plt.plot(data.p, data.speedup, label='Parallele Ausführung')

ax1.grid(True)
ax1.set_xlabel("Anzahl Threads")
ax1.set_ylabel("Speedup")
ax1.legend()