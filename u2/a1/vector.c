/*==============================================================================
  
   Purpose          : vector addition
   Author           : Rudolf Berrendorf
                      Computer Science Department
                      Bonn-Rhein-Sieg University of Applied Sciences
	              53754 Sankt Augustin, Germany
                      rudolf.berrendorf@h-brs.de
  
==============================================================================*/

#include <stdio.h>
#include <stdlib.h>

#include <pthread.h>
#include <libFHBRS.h>


//==============================================================================
// typedefs

// type for vector values
typedef short value_t;
// type for vector dimension / indices
typedef long index_t;
// function type to combine two values
typedef value_t (*function_t)(const value_t x, const value_t y);

typedef struct {
    value_t *a;
    value_t *b;
    value_t *c;
    index_t zuErledigendeTasks;
    pthread_mutex_t *mutex;
    value_t *sum;
    function_t f;
}threadInput;


//==============================================================================
/** @brief our function to combine two values
 * @param[in] x first value
 * @param[in] y secondd value
 * @return addition of the two values
 */
value_t add(const value_t x, const value_t y) {
  return ((x+y)*(x-y)) % ((int)x+1) + 27;
}


//==============================================================================
/** @brief initialize vectors
 * @param[in] n vector size
 * @param[out] a vector 1
 * @param[out] b vector 2
 * @param[out] c vector 3 (initialized with 0)
 */
void vectorInit(index_t n, value_t a[n], value_t b[n], value_t c[n]) {

  for(index_t i=0; i<n; i++) {
    a[i] = (value_t)(2*i);
    b[i] = (value_t)(n-i);
    c[i] = 0;
  }
}


//==============================================================================
/** @brief operate on two vectors sequentially
 * @param[in] n vector size
 * @param[in] a input vector 1
 * @param[in] b input vector 2
 * @param[out] c result vector
 * @param[in] f function to combine two values
 * @return sum of all vector elements in result vector
 */
value_t vectorOperation(index_t n, value_t a[n], value_t b[n], value_t c[n], function_t f) {

  value_t sum = 0;

  for(index_t i=0; i<n; i++) {
    sum += (c[i] = f(a[i], b[i]));
  }

  return sum;
}


//==============================================================================

void *threadFunction(void *arg) {
    threadInput input = * (threadInput *)arg;

    value_t summe = vectorOperation(input.zuErledigendeTasks, input.a, input.b, input.c, input.f);

    pthread_mutex_lock(input.mutex);
    *input.sum += summe;
    pthread_mutex_unlock(input.mutex);

    return EXIT_SUCCESS;
}


/** @brief combine two vectors in parallel
 * @param[in] n vector size
 * @param[in] a input vector 1
 * @param[in] b input vector 2
 * @param[out] c result vector
 * @param[in] f function to combine two values
 * @param[in] p number of threads to use
 * @return sum of all vector elements in result vector
 */
value_t vectorOperationParallel(index_t n, value_t a[n], value_t b[n], value_t c[n], function_t f, int p) {
    //n: länge der Vektoren
    //p: Anzahl der zu verwendenden Threads

    pthread_mutex_t *mutex;
    mutex = malloc(sizeof(pthread_mutex_t));
    if(mutex == NULL) {
        printf("Kein RAM verfügbar\n");
        exit(EXIT_FAILURE);
    }

    pthread_mutex_init(mutex, NULL);

    value_t *sum = malloc(sizeof(*sum));
    if(sum == NULL) {
        printf("Kein RAM verfügbar\n");
        exit(EXIT_FAILURE);
    }
    *sum = 0;

    long anzahlBenoetigterThreads = p;
    if (n<p) {
        anzahlBenoetigterThreads = n; // es werden nicht alle verfügbaren Threads benötigt
    }

    long anzahlThreadsMitEinerTaskMehr = n%p; //gilt n>p, dann werden die ersten n%p Threads eine Task mehr haben

    /*
     * ist p kein Teiler von n (also n%p > 0), dann wird der Divisionsrest hier weggeschmissen.
     * Das ist ok, weil der Rest mithilfe der Variable anzahlThreadsMitEinerTaskMehr abgehandelt wird.
    */
    long tasksProThread = n/p;

    pthread_t thr[p]; //thread identifiers
    int *status[p]; //thread exit status
    threadInput arg[p];

    long index = 0;

//    printf("n: %ld, ich verwende %ld/%d threads mit jeweils %ld tasks (+%ld)\n", n, anzahlBenoetigterThreads, p, tasksProThread,anzahlThreadsMitEinerTaskMehr);

    for (int thread = 0; thread < anzahlBenoetigterThreads; thread++) {
        arg[thread].zuErledigendeTasks = tasksProThread;

        if (anzahlThreadsMitEinerTaskMehr > 0) {
            arg[thread].zuErledigendeTasks += 1;
            anzahlThreadsMitEinerTaskMehr--;
        }

        arg[thread].a = &a[index];
        arg[thread].b = &b[index];
        arg[thread].c = &c[index];
        arg[thread].mutex = mutex;
        arg[thread].sum = sum;
        arg[thread].f = f;

        index += arg[thread].zuErledigendeTasks; // Arrayindex für nächsten Schleifendurchlauf updaten

        pthread_create(&thr[thread], NULL, threadFunction, (void *) &arg[thread]);
    }

    for (int thread = 0; thread < anzahlBenoetigterThreads; thread++) {
        pthread_join(thr[thread],  (void **) &status[thread]);
//        printf("Status Code ist %d\n", status[thread]);

    }

    int ret = *sum;
    free(sum);
    free(mutex);

    return ret;
}

//==============================================================================

int main(int argc, char **argv)
{
  // check for correct argument count
  if (argc != 3)
    {
      printf ("usage: %s vector_size n_threads\n", argv[0]);
      exit (EXIT_FAILURE);
    }

  // get arguments
  // vector size
  index_t n = (index_t)atol (argv[1]);
  // number of threads
  int p = atoi (argv[2]);
  // check for plausible values
  if((p < 1) || (p > 1000)) {
      printf("illegal number of threads\n");
      exit (EXIT_FAILURE);
  }

  // allocate memory
  value_t *a = malloc(n * sizeof(*a));
  value_t *b = malloc(n * sizeof(*b));
  value_t *c = malloc(n * sizeof(*c));
  if((a == NULL) || (b == NULL) || (c == NULL)) {
    printf("no more memory\n");
    exit(EXIT_FAILURE);
  }

  // initialize vectors a,b,c
  vectorInit(n, a, b, c);

  // work on vectors sequentially
  double t0 = gettime();
  value_t c1sum = vectorOperation(n, a, b, c, add);
  t0 = gettime() - t0;

  // work on vectors parallel for all thread counts from 1 to p
  for(int thr=1; thr<= p; thr++) {
    // do operation
    double t1 = gettime();
    value_t c2sum = vectorOperationParallel(n, a, b, c, add, thr);
    t1 = gettime() - t1;
    
    // check result
    if(c1sum != c2sum) {
      printf("!!! error: vector results are not identical !!!\nsum1=%ld, sum2=%ld\n", (long)c1sum, (long)c2sum);
      return EXIT_FAILURE;
    } else {
      // show timings
      printf("p=%2d, checksum=%2ld, sequential time: %9.6f, parallel time: %9.6f, speedup: %4.1f\n", thr, (long)c2sum, t0, t1, t0/t1);
    }
  }

  return EXIT_SUCCESS;
}

/*============================================================================*
 *                             that's all folks                               *
 *============================================================================*/
