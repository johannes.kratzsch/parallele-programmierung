Dies ist ein Programm, welches die sequentielle und parallele Ausführung von
Vektoroperationen miteinander vergleicht. In der Konsole wird der Speedup ausgegeben,
um den Performancegewinn durch die parallele Programmierung zu verdeutlichen.


Sie muessen pro Sitzung / Job-Lauf einmalig ausfuehren:
   module load gcc

Uebersetzen mit:
    make

Loeschen des ausfuehrbaren Programms:
    make clean

Testen auf Plausibilitaet mit kleinen Vektoren:
    make test

Ausfuehren von Messungen mit unterschiedlichen Threadzahlen auf grossen Vektoren
(  ***** NICHT AUF wr0!!! *****):
    make run

Starten des Job-Skripts:
    sbatch job_vector.sh
